package com.nobleprog.java.sec.files;

import java.io.*;

public class FilesManager {
    public static final String DATA_DIR = "userdata";
    public String fileRead(String fileName) throws IOException {
        FileReader reader = new FileReader(fileName);
        BufferedReader bufferReader = new BufferedReader(reader);
        String currentLine = null;
        StringBuilder stringBuilder = new StringBuilder();
        while ((currentLine = bufferReader.readLine()) != null){
            stringBuilder.append(currentLine);
        }
        stringBuilder.trimToSize();
        return stringBuilder.toString();
    }
    public void writeToFile(String fileName, String userInput) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(userInput);
        bufferedWriter.flush();
    }
}
