package za.vodacom.java.apis.FoodAppAPI;

import java.util.ArrayList;

public class Menu {
  ArrayList<Meal> mainMenu = new ArrayList<>();

  public void addAMeal(Meal newMeal){
    mainMenu.add(newMeal);
  }
  public Meal getAMealByName(String mealName){
    for(Meal currentMeal:mainMenu){
      if(currentMeal.getName().equalsIgnoreCase(mealName)) {
        return currentMeal;
      }
    }
    return null;
  }
  public Meal getAMealByIndex(int index){
    return mainMenu.get(index);
  }

  public ArrayList<Meal> getMainMenu() {
    return mainMenu;
  }

  public void setMainMenu(ArrayList<Meal> mainMenu) {
    this.mainMenu = mainMenu;
  }

  public ArrayList<Meal> getCheapMeals(){
    ArrayList<Meal> cheapMeal = new ArrayList<>();
    for(Meal currentMeal:this.mainMenu){
      if(currentMeal.getPrice() <= 20){
        cheapMeal.add(currentMeal);
      }
    }
    return cheapMeal;
  }
}
