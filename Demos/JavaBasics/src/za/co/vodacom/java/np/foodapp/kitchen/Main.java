package za.co.vodacom.java.np.foodapp.kitchen;

public class Main {
  public static void main(String[] args) {
    ModernMenu kidsMenu = new ModernMenu("kids");

    kidsMenu.addAMealToMenu(new Meal(10.0f, "fries"));
    kidsMenu.addAMealToMenu(new Meal(20.0f, "burger"));
    kidsMenu.addAMealToMenu(new Meal(15.0f, "fresh juice"));
    System.out.println(kidsMenu);

    Meal specialKidsMeal = new Meal(9.99f, "ChocoFestival");
    specialKidsMeal.setCalories(30);
    kidsMenu.addAMealToMenu(specialKidsMeal);
    System.out.println(kidsMenu);

    kidsMenu.removeMealFromMenu("burger");
    System.out.println(kidsMenu);

  }
}
