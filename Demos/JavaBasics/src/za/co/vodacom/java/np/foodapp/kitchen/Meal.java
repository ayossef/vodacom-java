package za.co.vodacom.java.np.foodapp.kitchen;

public class Meal {
    private float price;
    private double calories;
    private boolean vegan;
    private String name;
    public Meal(){
        super();
    }
    public Meal(float price, String name) {
        this.price = price;
        this.name = name;
    }

  public Meal(float price, double calories, String name) {
    this.price = price;
    this.calories = calories;
    this.name = name;
  }

  public void setCalories(double calories) {
        if(calories < 0 ){
            System.out.println("Eating doesn't burn calories, go work out");
        }else{
            this.calories = calories;
        }
    }
    public void setPrice(float price) {
        if(price < 0){
            System.out.println("Price can't be Negative");
            return;
        } else {
            this.price = price;
        }
    }
    public float getPrice() {
        return price;
    }



    public double getCalories() {
        return calories;
    }



    public boolean isVegan() {
        return vegan;
    }

    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Kitchen.Meal{" +
                "price=" + price +"RND"+
                ", calories=" + calories +
                ", vegan=" + vegan +
                ", name='" + name + '\'' +
                '}';
    }

    public void incPrice(){
        price = price +1; // add regA, 0x01; // 100 1001100 00000001
    }
    public void applyDiscount(float discountValue){
        price = price * discountValue;
    }

}
