package za.vodacom.np.java.foodapp;

import za.vodacom.np.java.foodapp.order.BevItem;
import za.vodacom.np.java.foodapp.order.BirthdayItem;
import za.vodacom.np.java.foodapp.order.FoodItem;
import za.vodacom.np.java.foodapp.order.Order;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // create a food item
        FoodItem pasta = new FoodItem("Pasta", 18);
        // create a bev item
        BevItem juice = new BevItem("Orange Juice", 12);
        // create a birthday item
        BirthdayItem balloons = new BirthdayItem("Balloons", 10, 1.5);
        // create an order
        Order myOrder = new Order();
        // add food, bev and birthday items to the order
       try{
           myOrder.addItem(pasta);
           myOrder.addItem(juice);
           myOrder.addItem(balloons);
           myOrder.addItem(new FoodItem("Pizza", 21.0));
           myOrder.addItem(new FoodItem("Fries", 130.0));
           myOrder.addItem(new FoodItem("Cake", 12.0));
       }catch (Exception ex){
           System.out.println(ex.getMessage());
       }

        // calculate the total order's price
        double totalPrice = myOrder.getTotalPrice();
        System.out.println(totalPrice);
    }
}