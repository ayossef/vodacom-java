package za.vodacom.MealsAPI;

public class Meal {
  private String name;
  private int price;
  private int cal;

  private String imgURL;

  public String getImgURL() {
    return imgURL;
  }

  public void setImgURL(String imgURL) {
    this.imgURL = imgURL;
  }

  public Meal(String name, int price, int cal) {
    this.name = name;
    this.price = price;
    this.cal = cal;
  }

  public Meal(String name, int price, int cal, String imgURL) {
    this.name = name;
    this.price = price;
    this.cal = cal;
    this.imgURL = imgURL;
  }

  @Override
  public String toString() {
    return "Meal{" +
      "name='" + name + '\'' +
      ", price=" + price +
      ", cal=" + cal +
      ", imgURL='" + imgURL + '\'' +
      '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getCal() {
    return cal;
  }

  public void setCal(int cal) {
    this.cal = cal;
  }
}
