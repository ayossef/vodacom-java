package za.vodacom.java.CRUDAPI;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.UUID;

import static com.fasterxml.jackson.databind.jsonFormatVisitors.JsonValueFormat.UUID;


@RestController
@RequestMapping("/api/v1/meals")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MealAPIController {

  ArrayList<Meal> allMeals = new ArrayList<>();

  @GetMapping
  public ArrayList<Meal> getAll(){
    return this.allMeals;
  }

  @GetMapping("/{id}") // /meals?id=1 -> /meals/1
  public Meal getMealById(@PathVariable UUID id){
   return mealWithId(id);
  }

  @PostMapping
  public Meal createMeal(@RequestBody Meal meal){
    this.allMeals.add(meal);
    return meal;
  }

  @PutMapping("/{id}")
  public Meal updateMeal(@PathVariable UUID id, @RequestBody Meal meal){
    this.allMeals.remove(mealWithId(id));
    this.allMeals.add(meal);
    return mealWithId(id);
  }

  @DeleteMapping("/{id}")
  public String deleteMeal(@PathVariable UUID id){
    Meal removedMeal = mealWithId(id);
    this.allMeals.remove(mealWithId(id));
    return "Meal "+removedMeal.getName()+" has been removed";
  }

  private Meal mealWithId(UUID id){
    for(Meal currentMeal:allMeals){
      if(currentMeal.getId().toString().equalsIgnoreCase(id.toString())){
        return currentMeal;
      }
    }
    return null;
  }

}
