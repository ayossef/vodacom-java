package com.vodacom.java.web.apidbdemo.controller;

import com.vodacom.java.web.apidbdemo.entities.User;
import com.vodacom.java.web.apidbdemo.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.processing.Generated;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersAPIController {
    @Autowired
    private final UserRepo repo;

    public UsersAPIController(UserRepo repo) {
        this.repo = repo;
    }

    @GetMapping
    public List<User> getAllUsers(){
        return repo.findAll();
    }

}
