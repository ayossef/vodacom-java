package com.vodacom.java.web.apidbdemo.repo;

import com.vodacom.java.web.apidbdemo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {
}
