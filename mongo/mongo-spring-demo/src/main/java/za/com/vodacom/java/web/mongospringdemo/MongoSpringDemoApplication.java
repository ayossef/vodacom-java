package za.com.vodacom.java.web.mongospringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import za.com.vodacom.java.web.mongospringdemo.entity.Product;

@SpringBootApplication
public class MongoSpringDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoSpringDemoApplication.class, args);
	}

}
