package za.com.vodacom.java.web.mongospringdemo.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.com.vodacom.java.web.mongospringdemo.entity.Product;

public interface ProductsRepo extends MongoRepository<Product, String> {
}
