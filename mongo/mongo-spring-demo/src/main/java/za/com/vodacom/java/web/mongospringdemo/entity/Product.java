package za.com.vodacom.java.web.mongospringdemo.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Product {
    @Id
    private String id;
    private String name;
    private String desc;
    private String imgUrl;
    private double rating;
    private double price;
}
