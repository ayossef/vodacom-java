package za.com.vodacom.java.web.mongospringdemo.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.com.vodacom.java.web.mongospringdemo.entity.Product;
import za.com.vodacom.java.web.mongospringdemo.repo.ProductsRepo;

import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api/v1/products")
@AllArgsConstructor
public class ProductsController {

    @Autowired
    private ProductsRepo productsRepo;

    @GetMapping("/")
    public List<Product> getAllProducts(){
        return productsRepo.findAll();
    }

    @GetMapping("/random")
    public Product createRandomProduct(){
        Product randomProduct = new Product();
        Random r = new Random();
        int iPhoneModel  = r.nextInt(4)+11;
        randomProduct.setName("iPhone "+iPhoneModel);
        randomProduct.setPrice(r.nextDouble(999));
        randomProduct.setDesc("The best iPhone "+iPhoneModel+ " so far");
        randomProduct.setRating(r.nextDouble(5));
        return productsRepo.insert(randomProduct);
    }
}
